---
title: "JRQZ's Site"

description: "梦里不知身是客，若知是梦何须醒"
# 1. To ensure Netlify triggers a build on our exampleSite instance, we need to change a file in the exampleSite directory.
theme_version: '2.8.2'
cascade:
  featured_image: '/img/jrqz/trail.jpg'
---
