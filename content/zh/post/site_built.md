+++
title = '基于Hugo的个人博客网站搭建'
date = 2024-04-01T22:03:58+08:00
draft = false
featured_image = ""
toc = true
+++

基于Hugo的个人博客网站，从初始化到申请自己的独立域名，开个博客记录一下

## 使用Hugo在本地创建个人网站

Hugo是一个基于Go语言的开源的静态网页生成器，以轻量快速闻名，[官网链接](https://gohugo.io/)

### 安装

平台：Ubuntu 22.04.2 LTS（Windows Subsystem for Linux 2）

#### 安装Go

安装Go，[官方教程](https://go.dev/doc/install)，安装完成后需要更新PATH，在`~/.bashrc`文件中添加

```bash
export PATH=$PATH:$HOME/go/bin
```

#### 安装Hugo

这里使用了从源码构建的方法

```bash
CGO_ENABLED=1 go install -tags extended github.com/gohugoio/hugo@latest
```

等待自动安装完毕，检查Hugo是否安装成功

```bash
hugo version
```

### 创建本地网站

Hugo建站非常简单，只需使用一条命令（将`jrqz`替换成你的项目名称）

```bash
hugo new site jrqz
```

刚建立的网站空空如也，所幸Hugo提供了一系列[主题（`theme`）](https://themes.gohugo.io/)可供快速上手，这里使用了和官方教程相同的[Anake主题](https://github.com/theNewDynamic/gohugo-theme-ananke?tab=readme-ov-file)

```bash
cd jrqz
git init
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
echo "theme = 'ananke'" >> hugo.toml
```

注：在以前的版本中，Hugo的配置文件名为`config.toml`，网上大部分教程仍旧如此

启动Hugo开发服务器，网站就创建成功了

```bash
hugo server
```

此时可以访问命令行提示的url查看网站

### 创建第一条博客

Hugo创建博客页面也非常简单，在相应位置下创建Markdown文件，hugo就可以自动将其转化为html页面

```bash
hugo new content posts/my-first-post.md
```

对应的文件可以看到如下内容：
```
+++
title = 'My First Post'
date = 2024-03-26T20:32:47+08:00
draft = true
+++
```

`draft = true`意味着Hugo默认不会发布这篇博客。方便起见，直接将其设置为`true`

`draft`设置为`true`后，再次执行

```bash
hugo server
```

访问页面，就可以看到发布的第一条博客了

## 将网站部署在Gitlab Pages上

拥有了本地网站之后，当然是要部署到服务器上让所有人都能访问。使用托管平台（如Github Pages, Vercel）是比较方便的选择。这里选择了Gitlab Pages，这样可以在远程从源代码自动化构建网站，并且不需要科学上网也能访问

首先，在Hugo项目根目录中创建GitLab CI/CD的配置文件`.gitlab-ci.yml`，它告诉GitLab如何构建和部署网站。文件内容如下

```
default:
  image: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo/hugo_extended:latest"

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  HUGO_ENV: production

before_script:
  - apk add --no-cache go curl bash nodejs
  ## Uncomment the following if you use PostCSS. See https://gohugo.io/hugo-pipes/postcss/
  #- npm install postcss postcss-cli autoprefixer

test:
  script:
    - hugo
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  environment: production
```

接着，要在`config.toml`中修改`baseURL`，格式为`https://{Gitlab用户名}.gitlab.io/{项目名}`，我这里为

```
baseURL = 'https://sznswjr.gitlab.io/jrqz/'
```

在[Gitlab](https://gitlab.com/)上创建项目，并把本地的Hugo项目关联到远程仓库上

```
git add .
git commit -m "Initial commit"
git remote add origin git@gitlab.com:sznswjr/jrqz.git
git push -u origin master
```

**注意**，GitLab 15.11会自动为项目的Gitlab Pages启用独立域名，需要在项目中的部署->Pages中取消勾选使用唯一域名，否则网站会出现资源定位问题，目前大部分教程都没有提到这点

等待Gitlab流水线构建完毕，访问`https://{Gitlab用户名}.gitlab.io/{项目名}`就能看到部署好的网站了

## 申请独立域名

首先需要在域名服务提供商购买域名，例如免费的[Freenom](https://www.freenom.com/) ，我这里选择的是[腾讯云](https://cloud.tencent.com/act/pro/domain-sale)，首年39

购买域名需要实名，而且要等待审核，我在工作日基本上不到一个小时就搞定了

等待的同时进入Gitlab的项目页面，进入部署->Pages->新域名，域名为你申请的`yoursite.com`

Gitlab会提供一个验证密钥

```
_gitlab-pages-verification-code.yoursite.com TXT gitlab-pages-verification-code=xxxxxxxxxxxxxxxxxxxxxxxxxxx
```

域名注册好后，进入域名提供商的控制台，在域名解析中添加如下三条记录

| 主机记录 | 记录类型 | 记录值                    |
| -------- | -------- | ------------------------- |
| @        | CNAME    | {Gitlab用户名}.gitlab.io. |
| www      | CNAME    | {Gitlab用户名}.gitlab.io  |
| @        | TXT      | {Gitlab提供的验证密钥}    |

这三条DNS记录使用户输入的是裸域和`www`子域都能正确访问到GitLab Pages托管的网站，并通过TXT记录验证域名所有权，满足GitLab Pages设置自定义域名的要求

到这里还有最后一步，那就是在你的`hugo.toml`文件中将`baseURL`修改为新的域名，否则同样会出现资源定位问题。修改后推送到远程仓库，待流水线构建完毕就可以使用自己申请的`yoursite.com`访问你的网站了

