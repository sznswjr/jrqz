+++
title = 'Hive上手'
date = 2024-09-14T16:14:42+08:00
draft = false
featured_image = ""
toc = true
post_content_classes = "BlinkMacSystemFont bg-near-white"
+++

参考：《Hive编程指南》第一版，[Hive官方wiki](https://cwiki.apache.org/confluence/display/Hive/GettingStarted)，https://bbs.huaweicloud.com/blogs/197920

《Hive编程指南》第一版出版于2013年，此时hive2尚未发布，如今hive3已经普及，很多知识已经过时，这里仅为学习



Hive是一个开源的**数据仓库框架**，基于Hadoop生态系统，用于处理和分析大量的结构化和半结构化数据。

Hive提供了一个类似于SQL的查询语言，称为HiveQL（Hive Query Language），允许用户编写查询来分析存储在Hadoop分布式文件系统（HDFS）或其他兼容的存储系统（如Amazon S3）中的数据。HiveQL查询会被转换成一系列的MapReduce作业，然后在Hadoop集群上执行。Hive还支持其他执行引擎，如Apache Tez和Apache Spark。

{{< image src="/img/jrqz/hive.png">}}

Hive发行版中附带的模块有CLI，一个成为Hive网页界面（HWI）的简单网页界面，以及可通过JDBC、ODBC和一个Thrift服务器（参考第16章）进行编程访问的几个模块

所有的命令和查询都会进入到Driver（驱动模块），通过该模块对输入进行解析编译，对需求的计算进行优化，然后按照指定的步骤执行。当需要生成MR任务（job）时，Hive通过一个表示”job执行计划“的XML文件驱动执行内置的、原生的Mapper和Reducer模块。

Hive通过和JobTracker通信来初始化MR任务，而不必部署在JobTracker所在节点

Metastore是一个独立的关系型数据库，Hive会在其中保存表模式和其它系统源数据



## 单节点安装和使用HIVE

### 准备

java 1.8

hadoop 3.4.0

hive安装包https://dlcdn.apache.org/hive/，版本3.1.3

```shell
wget https://dlcdn.apache.org/hive/hive-3.1.3/apache-hive-3.1.3-bin.tar.gz
tar -xzvf apache-hive-3.1.3-bin.tar.gz
```

### MySQL安装与配置

hive的使用metastore保存数据元信息，这里使用mysql

卸载机器自带mysql：

```shell
yum remove mariadb mariadb-libs
```

安装：

```shell
yum install TXSQL-client.x86_64
```

启动mysql：

```shell
# 启动mysql
sudo service mysql start

# 测试登录，mysql5.7默认root用户初始密码为空
mysql -u root
```

此时有权限问题无法登录，进行如下步骤：

```shell
# 停止mysql服务
sudo service mysql stop
# 以安全模式启动MySQL
sudo mysqld_safe --skip-grant-tables &
# 登录mysql
mysql -u root
```

修改root用户密码

```mysql
update mysql.user set authentication_string=PASSWORD('newPwd'), plugin='mysql_native_password' where user='root';
flush privileges;
```

重启服务

```shell
sudo service mysql stop
sudo service mysql start
# 此时可以用密码登录
mysql -u root -p
```

配置完hive后，可查看元数据信息：

```shell
mysql> use hive;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> show tables;
+-------------------------------+
| Tables_in_hive                |
+-------------------------------+
| AUX_TABLE                     |
| BUCKETING_COLS                |
| CDS                           |
| COLUMNS_V2                    |
| COMPACTION_QUEUE              |
| COMPLETED_COMPACTIONS         |
| COMPLETED_TXN_COMPONENTS      |
| CTLGS                         |
| DATABASE_PARAMS               |
| DBS                           |
| DB_PRIVS                      |
| DELEGATION_TOKENS             |
| FUNCS                         |
...
```

### Hive安装和配置

hive环境变量配置，写入~/.bashrc：

```shell
export HIVE_HOME=/path/to/hive                                                                     
export PATH=$HIVE_HOME/bin:$PATH
```

在当前会话启用配置

```shell
source ~/.bashrc
```

进入hive的conf目录，复制生成hive-site.xml文件

```shell
cp hive-default.xml.template hive-site.xml
```

编辑hive-site.xml文件

```xml
# 此项务必配置，否则之后进入hive命令行会报错
<property>
  <name>system:java.io.tmpdir</name>
  <value>/tmp/hive/java</value>
</property>
<property>
  <name>system:user.name</name>
  <value>${user.name}</value>
</property>

# MySQL相关配置
<property>
  <name>hive.metastore.db.type</name>
  <value>MYSQL</value>
</property>
<property>
  <name>javax.jdo.option.ConnectionUserName</name>
  <value>root</value>
</property>
<property>
  <name>javax.jdo.option.ConnectionPassword</name>
  <value>123456</value>
</property>
<property>
  <name>javax.jdo.option.ConnectionDriverName</name>
  <value>com.mysql.jdbc.Driver</value>
</property>
<property>
  <name>javax.jdo.option.ConnectionURL</name>
  <value>jdbc:mysql://localhost:3306/hive?createDatabaseIfNotExist=true&amp;characterEncoding=UTF-8&amp;useSSL=false</value>
</property>
```

MySQL Connector

在[Maven仓库](https://mvnrepository.com/artifact/mysql/mysql-connector-java/5.1.38)下载速度较快，这里使用mysql-connector-java-5.1.38.jar，下载并移至hive的lib目录下

```shell
wget https://repo1.maven.org/maven2/mysql/mysql-connector-java/5.1.38/mysql-connector-java-5.1.38.jar
```

初始化Schema

```
schematool -dbType mysql -initSchema

# 报错1：java.lang.NoSuchMethodError: com.google.common.base.Preconditions.checkArgument
# 原因：hadoop路径下的guava包与hive的lib下的guava包版本冲突
# 解决办法：删除hive的lib下的低版本的guava包

# 报错2：Illegal character entity: expansion character (code 0x8 at [row,col,system-id]: [3215,96,"file:/……/hive-site.xml"]
# 原因：hive-site.xml一些字符不符合xml文档格式规范（一般在description字段）
# 解决办法：根据指示删除相应字符即可
```

测试Hive（先启动hadoop）

```shell
# 进入hive命令行
hive
# 创建表
create table test(id int);

# 退出hive命令行，查看hadoop是否创建相应目录，如果存在/user/hive/warehouse/test说明创建成功
hdfs dfs -ls /user/hive/warehouse
```



metastore

内表（管理表）/外表

分区目录

