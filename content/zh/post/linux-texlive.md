+++
title = 'linux安装texlive最简单方法'
date = 2024-10-15T14:06:01+08:00
draft = false
featured_image = ""
toc = true
post_content_classes = "BlinkMacSystemFont bg-near-white"
+++

### 前置准备

安装docker，这里略

### 使用docker安装texlive

可以在dockerhub上找到官方镜像https://hub.docker.com/r/texlive/texlive

拉取镜像并启动容器：

```
docker pull texlive/texlive
docker run -it --name latex texlive/texlive:latest /bin/bash
```

### 使用vs code在latex docker环境中编辑

方法1：直接进入容器

vs code安装Dev Containers扩展，左下角点击远程环境，选择Attach to running Container，打开工作目录即可

方法2：容器开放ssh供远程链接

注：下面方法相当于重新基于镜像启动了一个新容器

```
docker run -d -it -p 36001:22 --name latex texlive/texlive:latest /bin/bash
service ssh start
# 用`Ctrl+P+Q`命令保持后台运行退出容器
```

vs code远程ssh链接36001端口即可

