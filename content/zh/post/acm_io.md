+++
title = 'ACM模式C++常用io'
date = 2024-04-12T11:53:47+08:00
draft = false
featured_image = ""
toc = true
post_content_classes = "BlinkMacSystemFont bg-near-white"
+++

自用，ai生成

提前引入头文件

```cpp
#include <bits/stdc++.h>
```

### cin

`cin`是C++中处理标准输入的对象，属于`iostream`库。它用于从标准输入流（通常是键盘）读取数据。

`cin`会在等待用户输入时阻塞程序执行。具体来说，它在以下几种情况下会阻塞：

- **等待输入**：当程序到达读取输入的语句，且标准输入中没有足够的数据时，`cin`会阻塞程序，等待用户输入数据并按下回车键。
- **输入缓冲区不空，但数据不足**：如果输入缓冲区中有一部分数据，但不足以满足当前读取操作的需要，`cin`同样会阻塞，等待更多输入。

`cin`用作布尔表达式时，能够反映输入操作的状态：

- 当输入成功时，`cin`表达为`true`。
- 如果遇到输入结束（EOF），或者输入类型与变量类型不匹配（例如，尝试将字母读入整型变量），`cin`会表达为`false`。

`cin`, 作为C++标准库中的输入流对象，可以用来读取多种基本数据类型，以及一些库中定义的复合类型。这里列出了`cin`可以直接使用的一些常见数据类型：

基本数据类型

- **整数类型**：`int`, `short`, `long`, `long long`，以及它们的无符号版本 `unsigned int`, `unsigned short`, `unsigned long`, `unsigned long long`。
- **浮点数类型**：`float`, `double`, `long double`。
- **字符类型**：`char`，以及`unsigned char`和`signed char`。
- **布尔类型**：`bool`。

复合类型

- **字符串**：`std::string`。读取时，`cin`将会读取并忽略任何前导空格（包括空格、换行符等），然后读取字符直到下一个空白字符为止。

对于大多数基本数据类型（如`int`、`double`、`char`等），`cin`使用`>>`操作符读取数据时，会自动跳过任何前导的空白字符，包括空格、制表符和换行符。这意味着如果输入缓冲区的开始是空白字符，`cin >>`会忽略它们，直到找到第一个非空白字符，并从那里开始读取输入。

### getchar()

`getchar()`函数会从输入流中读取下一个字符，并且这个字符会被消耗掉，即它会从输入流中移除，不再可用于后续的输入操作。这意味着一旦`getchar()`读取了一个字符，这个字符就不会再出现在输入流中，无论是`cin`、`scanf`还是其他任何读取标准输入的函数，都无法再次访问到这个字符。

### getline()

`std::getline()` 是 C++ 标准库中的一个函数，它用于从输入流中读取一行文本。`std::getline()` 主要与 `std::istream` 类型（如 `std::cin`、文件流等）一起使用，能够从给定的输入流中读取数据直到遇到换行符（`'\n'`），并将读取的内容（不包括换行符）存储到一个 `std::string` 或其他字符序列中。

`std::getline()` 有几个重载版本，最常用的两个版本的签名如下：

1. 从标准输入流读取：

   ```
   std::istream& getline(std::istream& is, std::string& str, char delim = '\n');
   ```

   - `is`: 输入流（如 `std::cin` 或文件输入流）。
   - `str`: 存储从输入流中读取的行的字符串。
   - `delim`: 可选参数，指定作为行结束符的字符，默认是换行符 `'\n'`。函数会读取数据直到遇到此字符为止。

2. 从字符串流读取（属于 `<sstream>` 头文件）：

   ```
   std::istream& getline(std::stringstream& ss, std::string& str, char delim = '\n');
   ```

   - 这个版本与上面的版本类似，只是输入流是字符串流 `std::stringstream`。

**`std::getline()` 会从输入流中消耗（读取并移除）分隔符**

`std::getline()` 不会跳过空格。当从输入流中读取一行数据时，它会包含该行中的所有字符，直到遇到分隔符（默认为换行符`\n'`）为止，但不包括分隔符本身。这意味着空格、制表符等空白字符都会被作为字符串的一部分读取并保存

### istringstream&stringstream

`istringstream`和`stringstream`都是C++标准库中提供的字符串流，它们都继承自`ios_base`类，并提供对内存中字符串的读写操作。不过，它们之间有一些关键的区别，主要体现在用途和功能上。

istringstream

- `std::istringstream`是输入字符串流，主要用于从字符串中读取数据。
- 它只支持输入操作，即可以从一个字符串中提取（读取）数据，类似于如何从文件或标准输入中使用`std::cin`读取数据。
- 用途示例：解析字符串，将字符串内容转换为其他数据类型（如整数、浮点数等）。

stringstream

- `std::stringstream`同时支持输入和输出操作，是一种双向字符串流。
- 它可以用于从字符串读取数据（输入），也可以向字符串中写入数据（输出）。
- `std::stringstream`更加灵活，适用于需要同时读写字符串的场景。
- 用途示例：读取字符串数据，对数据进行处理或格式化后再写回到另一个字符串。

使用场景对比

- 当你仅需要从一个字符串中读取数据时，使用`std::istringstream`是一个更加轻量级的选择，因为它专门为输入设计。
- 当你需要对一个字符串进行读写操作时，`std::stringstream`提供了更大的灵活性和功能。

示例

**istringstream 示例：**

```
#include <iostream>
#include <sstream>
#include <string>

int main() {
    std::string data = "123 45.67";
    std::istringstream iss(data);
    int intValue;
    double doubleValue;
    iss >> intValue >> doubleValue;
    std::cout << intValue << ", " << doubleValue << std::endl; // 输出: 123, 45.67
}
```

**stringstream 示例：**

```
#include <iostream>
#include <sstream>
#include <string>

int main() {
    std::stringstream ss;
    ss << 123 << ' ' << 45.67; // 写入数据到ss
    std::string data = ss.str(); // 从ss中获取字符串
    std::cout << data << std::endl; // 输出: 123 45.67
    
    int intValue;
    double doubleValue;
    ss >> intValue >> doubleValue; // 读取数据，需要重置或清空ss
    std::cout << intValue << ", " << doubleValue << std::endl;
}
```

