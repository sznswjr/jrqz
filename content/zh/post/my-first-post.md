+++
title = 'My First Post'
date = 2024-03-26T20:32:47+08:00
categories = [
    "hugo",
]
toc = true
draft = false
featured_image = ""
+++

## JRQZ的第一篇博客

蔚蓝天空——

将一切吞噬——

即使如此命运的齿轮也不会停止转动——

![图片](/img/jrqz/laiwei.jpg)
