+++
title = 'Linux服务器配置ES+Kibana'
date = 2024-09-14T14:28:47+08:00
draft = false
featured_image = ""
toc = true
post_content_classes = "BlinkMacSystemFont bg-near-white"
+++

踩坑记录

依旧是参考官网教程：https://www.elastic.co/guide/en/elasticsearch/reference/current/run-elasticsearch-locally.html

## 准备

云服务器配置：

```
8核；16G；系统盘:100G；数据盘:500G
TencentOS Server
```

ES，Kibana分别部署在docker上

## 安装

安装docker，略

配置环境变量：

```shell
export ELASTIC_PASSWORD="<ES_PASSWORD>"  # password for "elastic" username
export KIBANA_PASSWORD="<KIB_PASSWORD>"   # Used _internally_ by Kibana, must be at least 6 characters long
```

安装es

```shell
docker network create elastic-net

docker run -p 0.0.0.0:9200:9200 -d --name elasticsearch --network elastic-net \
  -e ELASTIC_PASSWORD=$ELASTIC_PASSWORD \
  -e "discovery.type=single-node" \
  -e "xpack.security.http.ssl.enabled=false" \
  -e "xpack.license.self_generated.type=trial" \
  docker.elastic.co/elasticsearch/elasticsearch:8.14.2
```

安装Kibana

```shell
# configure the Kibana password in the ES container
curl -u elastic:$ELASTIC_PASSWORD \
  -X POST \
  http://localhost:9200/_security/user/kibana_system/_password \
  -d '{"password":"'"$KIBANA_PASSWORD"'"}' \
  -H 'Content-Type: application/json'
  
docker run -p 0.0.0.0:4601:5601 -d --name kibana --network elastic-net \
  -e ELASTICSEARCH_URL=http://elasticsearch:9200 \
  -e ELASTICSEARCH_HOSTS=http://elasticsearch:9200 \
  -e ELASTICSEARCH_USERNAME=kibana_system \
  -e ELASTICSEARCH_PASSWORD=$KIBANA_PASSWORD \
  -e "xpack.security.enabled=false" \
  -e "xpack.license.self_generated.type=trial" \
  docker.elastic.co/kibana/kibana:8.14.2
```

这里有两个坑：

1. 设置端口转发时，主机地址不能设置成127.0.0.1，必须设置成0.0.0.0才能保证外网能访问
2. Kibana的端口转发主机端设置成5601时外网会无法访问（原因暂不明，推测是其它进程占用端口）

## 测试

ES

```shell
curl -u elastic:$ELASTIC_PASSWORD 127.0.0.1:9200
# 在本地测试时，把127.0.0.1换成主机ip即可
```

Kibana

浏览器访问xxx.xxx.xxx.xxx:4601

## 踩坑记录

### 外网无法访问服务

创建docker设置端口转发时，需要将127.0.0.1修改成0.0.0.0，表示监听所有可用ipv4地址

### Kibana无法访问

此时设置的端口转发为0.0.0.0:5601:5601

浏览器访问xxx.xxx.xxx.xxx:5601无响应

服务器端输入

```shell
curl 127.0.0.1:5601
```

立即返回，使用-v参数排查，说明需要登入密钥

重启es，不设置Kibana的用户密码，此时再运行上述命令即可返回页面html，说明服务器端可以正常访问服务

本地输入

```shell
curl xxx.xxx.xxx.xxx:5601
```

发现无响应

论坛上搜索相似问题，大部分文章提到修改Kibana的配置文件`\usr\share\elasticsearch\config\elasticsearch.yml`：

```
network.host: 0.0.0.0
```

但是实际上配置文件已经设置了这一行

此时的情况：本地和服务器端均可访问es，服务器端可访问Kibana，本地不可访问Kibana

这说明网络连通没问题，可能是Kibana的5601端口不能接收数据

查看双端的防火墙安全设置，并没有相关的规则，关闭防火墙也不行

使用tcpdump工具查看服务器端5601端口的数据包收发情况

```shell
tcpdump -i any -nn port 5601
```

发现本机发送请求时没有数据包，说明Kibana根本没有收到数据

尝试换绑端口到4601，外网成功连通

此时浏览器访问，提示Kibana server is not ready

这是因为之前没有设置ES的Kibana账号密码，重新设置并重启ES服务，问题解决

