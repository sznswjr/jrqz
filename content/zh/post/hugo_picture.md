+++
title = 'Hugo嵌入图片的几种方式'
date = 2024-04-08T23:29:52+08:00
draft = false
toc = true
featured_image = ""
+++

### 使用Markdown语法

```
![图片替代文字](/img/jrqz/trails_in_the_sky_sc.jpg)
*图片说明文本*
```
不支持调整大小

![图片替代文字](/img/jrqz/trails_in_the_sky_sc.jpg)
*图片说明文本*

### 使用HTML

```
<figure>
    <img src="/img/jrqz/trails_in_the_sky_sc.jpg" alt="图片替代文字" width="50%">
    <figcaption>这里是图片说明</figcaption>
</figure>
```

需要在hugo.toml（config.toml）中添加设置
```
[markup.goldmark.renderer]
unsafe = true
```

<figure>
    <img src="/img/jrqz/trails_in_the_sky_sc.jpg" alt="图片替代文字" width="50%">
    <figcaption>这里是图片说明</figcaption>
</figure>

可能会导致代码注入等安全问题

### 使用Hugo短代码

```
{{</* figure src="/img/jrqz/trails_in_the_sky_sc.jpg" caption="这里是图片说明" */>}}
```

更改图片大小需要定义css样式，较为麻烦

{{< figure src="/img/jrqz/trails_in_the_sky_sc.jpg" caption="这里是图片说明" >}}

